FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > seahorse.log'

COPY seahorse .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' seahorse
RUN bash ./docker.sh

RUN rm --force --recursive seahorse
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD seahorse
